// ***********************
// Configurable parameters
// ***********************

.const	speed		=	2.5*256		// Speed

// Robot
.const	col0	=	[cmdLineVars.get("color").asNumber()&$f]
.const	col1	=	floor(cmdLineVars.get("color").asNumber()/16)&$f
.const	col2	=	floor(cmdLineVars.get("color").asNumber()/256)&$f
.const	col3	=	floor(cmdLineVars.get("color").asNumber()/4096)&$f


// Don't touch
// ***********

.const	d016val		=	$c0+[[cmdLineVars.get("multi").asNumber()==1]?$10:$00]
.const 	sizex = floor(cmdLineVars.get("sizex").asNumber()/8)+[[cmdLineVars.get("sizex").asNumber()&7]!=0?1:0]
.const	sizey = floor(cmdLineVars.get("sizey").asNumber()/8)+[[cmdLineVars.get("sizey").asNumber()&7]!=0?1:0]
	
.var	defblock=List().add($4800+[$400-8*sizex],$5000+[$400-8*sizex],$5800+[$400-8*sizex],$6000+[$400-8*sizex],$6800+[$400-8*sizex],
							$7000+[$400-8*sizex],$c800+[$400-8*sizex],$d000+[$400-8*sizex],$d800+[$400-8*sizex],$e000+[$400-8*sizex],
							$e800+[$400-8*sizex],$f000+[$400-8*sizex],$f800+[$400-8*sizex],
							$4400,$4c00,$5400,$5c00,$6400,$6c00,$7400,
							$c400,$cc00,$d400,$dc00,$e400,$ec00,$f400)
					
// *******************
// Zero page variables
// *******************

.const	savea	=	$80	// Save A   (IRQ/NMI)
.const	savex	=	$81	// Save X   (IRQ/NMI)
.const	savey	=	$82	// Save Y   (IRQ/NMI)
.const	save1	=	$83	// Save $01 (IRQ/NMI)

.const	posy	=	$84	// Y real value  (5.3) - 24 BIT
.const	posx	=	$87	// X real value (13.3) - 24 BIT
.const	tmp		=	$8a	// Temporary value used in "Drawscreen"

.const	coltmp	=	$8c	// Temporary column value
.const	colprc	=	$8d	// Preceding column value
.const	rowtmp	=	$8e	// Temporary row value used inside NMI
.const	rowend	=	$8f	// End row value used inside NMI
.const	rowreal	=	$90	// Actual row value used outside NMI	
.const	rowchg	=	$91	// Trigger a row decrunch

.const	d011dbl	=	$92	// Avoid badline
.const	d011val	=	$93	// Correct value
.const	tmppos	=	$94	// Temporary position
.const	stackmn	=	$95	// Save stack for decrunching main section
.const	pageval	=	$96	// D018/DD00 value to be applied every 8 lines
.const	startnmi=	$97	// NMI countdown
.const	misc	=	$98	// Miscellaneous use
.const	decidx	=	$99	// Decruncher index
.const	actidx	=	$9a // Actual decruncher index





// ***
// SID
// ***
// Must be inside $1000-$1fff

.var sid = LoadSid("..\sid\lame.sid")
.pc=sid.location "Music" .fill sid.size, sid.getData(i)

// *********
// Main code
// *********
.pc=$0800 "Code"
start:
	sei
	lda #$34
	sta $01	
	jsr initvar
	lda #$00
	jsr loadfstscreen
	lda #$35
	sta $01	
	jsr setcolors
	lda #$00
	jsr sid.init
	jsr disableTimers	// Disable CIA 2 timers			
	jsr setfstirqnmi	
	lda #$34
	sta $01
	cli
	
	
// waitloop
// ********
// Main code: sit&wait, load fonts when needed		

waitloop:
	ldy actidx
	cpy decidx
	beq waitloop	
	ldx declist,y
	iny
	tya
	and #$0f
	sta actidx
	txa
	jsr getfont
	jmp waitloop
	
	
	
// Initvar
// *******
// Initialize mlain variables
initvar:	
	lda #$00		
	sta decidx
	sta actidx
	sta rowreal	
	sta posx	
	sta posx+1
	sta posx+2
	sta posy
	sta posy+1
	sta posy+2
	lda #$01	
	sta colprc
	rts
	
	
// Update position
// ***************
// Update current position based on Joy port 2
updposition:	
	ldx #$00	
	lda $dc00
	eor #$ff
	tay
	and #$08
	beq noleft
	clc
	lda posx
	adc #<speed
	sta posx
	lda posx+1
	adc #>speed
	sta posx+1
	lda posx+2
	adc #$00
	sta posx+2
	cmp #>[8*[sizex-39]]
	bcc noleft
	lda posx+1
	cmp #<[8*[sizex-39]]
	bcc noleft
	lda #>[8*[sizex-39]-1]
	sta posx+2
	lda #<[8*[sizex-39]-1]
	sta posx+1
noleft:
	tya
	and #$04
	beq noright
	sec
	lda posx
	sbc #<speed
	sta posx
	lda posx+1
	sbc #>speed
	sta posx+1
	lda posx+2
	sbc #$00
	sta posx+2
	bcs noright
	lda #$00
	sta posx+1
	sta posx+2
noright:
	tya
	and #$02
	beq noup
	clc
	lda posy
	adc #<speed
	sta posy
	lax posy+1
	adc #>speed
	sta posy+1
	txa	
	ldx #0
	eor posy+1
	and #$08
	beq norowinc
	inx
norowinc:
	lda posy+2
	adc #$00
	sta posy+2
	cmp #>[[sizey-24]*8]
	bcc nodown
	lda posy+1
	cmp #<[[sizey-24]*8]
	bcc noup
	lda #>[[sizey-24]*8-1]
	sta posy+2
	lda #<[[sizey-24]*8-1]
	sta posy+1
	ldx #0
noup:
	tya
	and #$01
	beq nodown
	sec
	lda posy
	sbc #<speed
	sta posy	
	lax posy+1
	sbc #>speed
	sta posy+1	
	txa
	ldx #0
	eor posy+1
	and #$08
	beq norowdec
	dex
norowdec:		
	lda posy+2
	sbc #$00
	sta posy+2
	bcs nodown
	lda #$00
	sta posy+2
	sta posy+1
	ldx #0
nodown:	
	clc		
	txa
	stx rowchg
	adc rowreal		
	bpl !+
	adc #27
!:
	cmp #27
	bcc !+
	sbc #27
!:
	sta rowreal
	sta rowtmp	
	tax
	lda dxxxvalues,x	
	sta pageval
	dex
	dex		
	bpl !+
	txa
	axs #[$100-27]
!:	
	stx rowend			// row before the first one
	lda dxxxvalues,x
	sta $d018
	asl
	sta $dd00
	rts

	
	
// Load first screen
// *****************
// Load full screen starting from row <A>
loadfstscreen:
	ldx #26
	stx misc
!:		
	jsr getfont
	clc
	adc #$01
	dec misc
	bne !-
	rts	
	
// getfont
// *******
// Load single row as font
// A: row to load	
getfont:
	pha
	asl		
	php
	clc
	adc #<rowindex
	sta tmp
	lda #>rowindex
	adc #$00
	plp
	adc #$00
	sta tmp+1
	ldy #$01
	lax (tmp),y
	dey
	lda (tmp),y
	tay
	jsr Decrunch
	pla
	rts

	
.import source "irq.asm"			// Raster IRQ handler routines (SID + Management)
.import source "nmi.asm"			// NMI handler (Soft badlines)
.import source "drawscreen.asm"		// Update charscreen
.import source "timer.asm"			// Timer management
.import source "decruncher.asm"		// Byteboozer decruncher (by HCL)


// **************
// * VIC tables *
// **************
.align $0020
.pc=* "D018/DD00 table"
dxxxvalues:
.print defblock.size()
.for(var i=0;i<defblock.size();i++)
	.byte floor([defblock.get(i)&$3800]/$400)+[[defblock.get(i)>>15]^1]
	
.pc=* "Screen row ptr"
scrptrlo:
.for(var i=0;i<25;i++)
 .byte <[40*i]
scrptrhi:
.for(var i=0;i<25;i++)
 .byte [$40]+[>[40*i]]

.align $0020 
.pc=* "Screen bank table"
scrbank:
.for(var i=0;i<defblock.size();i++)
	.byte >[defblock.get(i)&$c000]

.align $0020
.pc=* "Row increment table"
rowinc:
.fill defblock.size(),mod(i+1,27)

.align $0010
.pc=* "Row update helper table"
rowupd:
.var bnk
.var exbnk	=	-1
.var rowhelp=	List()
.var cnt
.for(var i=defblock.size()-1;i>=0;i--) {
	.eval bnk=[defblock.get(i) & $400]!=0 ? $80:$00
	.if (bnk!=exbnk) .eval cnt=1
	.eval exbnk=bnk
	.eval rowhelp.add(bnk+cnt)
	.eval cnt++
	}
.for(var j=0;j<2;j++)
	.for(var i=rowhelp.size()-1;i>=0;i--) 
		.byte rowhelp.get(i)
		
.align $0020
.pc=* "Badline table"
rowd011:
.var	dsp=0
.for(var i=rowhelp.size()-1;i>=0;i--) {
	.if (dsp<rowhelp.get(i)) .byte BIT_ABS
						else .byte STA_ABS
	.eval dsp=rowhelp.get(i)
}

.pc=* "Decruncher list"
declist:
.fill 16,0

.pc=* "Row index"
rowindex:
.import binary "..\bin\index.bin"

.pc=$0280 "Compressed data block 1"
.import binary "..\bin\stuff0280.bin"

.pc=$2000 "Compressed data block 2"
.import binary "..\bin\stuff2000.bin"
 

 