// NMI handler
// ***********
// This is used to change d018/dd00 every 8 lines
// Bad lines are triggered:
// - In the first row of the screen
// - Every 16 rows
.align $0040
.pc=* "NMI handler"
nmiwt:				// Wait	
	sta savea
	lda $01
	sta save1
	lda #$35
	sta $01
	bit $dd0d		// This handler just sits and wait
	dec startnmi
	bne !+				
	lda #<nmibl		// As soon as the right row is reached the effect starts
	sta $fffa	
	lda #BIT_ABS
	sta d011use
!:
	lda save1
	sta $01
	lda savea
	rti	
	
nmibl:				// NMI handler for skipping Badlines	
	sta savea		// 3 + 6 + (9 + 0-7) => 18 (+0-7)		
	stx savex		// 3	
	lda $01			// 3
	sta save1		// 3	
	ldx #$35		// 2
	stx $01			// 3
	sec				// 2	
	lda #$d7		// 2
	sbc $dd04		// 4
	sta *+4			// 4
	bpl *			// 3 (+7-0)
	lda #$a9
	lda #$a9
	lda #$a9
	lda $eaa5
.if ([>*]!=[>nmiwt]) .error "NMI page crossing!"				
	lda d011dbl		// 52 - Ok, synced
d011use:	
	sta $d011		// 54
	lda pageval		// 61
	sta	$d018		// 2
	asl
	sta $dd00		// 6
	cli
	ldx rowtmp
	lda rowinc,x
	tax
	sta rowtmp
	lda rowd011,x
	sta d011use
	lda dxxxvalues,x
	sta pageval			
	cpx rowend
	bne nmiend
	lda #$7f
	sta $dd0d	
nmiend:
	bit $dd0d
	ldx savex	
	lda d011val		// 54 - This must be here!
	sta $d011		
	lda save1
	sta $01	
	lda savea	
	rti
