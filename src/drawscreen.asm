// Set colors
// **********
setcolors:
	lda #col1
	ldx #$00
!:
	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00,x
	dex
	bne !-
	lda #col0
	sta $d021
	lda #col2
	sta $d022
	lda #col3
	sta $d023	
	rts
	
// Draw screen
// ***********
// X: x position (0-87)
// Y: y position (0-31)
.pc=* "Drawscreen"
drawscreen:	
	txa				// Fix X
	axs #[$100-39]	// Fix X
	lda rowupd,y
	cmp #$80
	and #$7f
	sta tmp	
	bcs !+
	lda #128-sizex
	sta dsadder1+1
	lda #128
	sta dsadder2+1
	jmp dscont
!:
	lda #128-sizex
	sta dsadder2+1
	lda #128
	sta dsadder1+1	
dscont:
	lda scrbank,y
	sta block10+2
	tya
	ora #<rowupd
	sta ds2ndrow+1
	ldy tmp	
	lda scrptrlo,y
	sta block12+1	
	lda scrptrhi,y
	sta block12+2
	clc	
	tya
ds2ndrow:
	adc rowupd,y	
	and #$7f
	tay
	lda scrptrlo,y
	sta block11+1
	lda scrptrhi,y
	sta block11+2	
	lda #BIT_ABS			// avoid third STA...
	cpy #25
	bcs upd2rows
upd3rows:
	eor #STA_ABSY^BIT_ABS	// do third STA if row<25
upd2rows:
	sta block11	
drawstart:
	ldy #39
	clc
drawloop:
	txa
dsadder1:
	adc #128-sizex
block10:
	sta $0000,y
block11:
	sta $0000,y
	txa
dsadder2:
	adc #128
block12:	
	sta $0000,y
	dex
	dey 
	bpl drawloop
	rts
