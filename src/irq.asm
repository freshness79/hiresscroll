// **********	
// SID player
// **********
// Schedule on the top of the screen
irq_sidplayer:
	pha
	txa
	pha
	tya
	pha
	lda $01
	pha
	lda #$35
	sta $01
	lsr $d019	
	cli
	jsr setbotirq
	jsr sid.play	
	pla
	sta $01
	pla
	tay
	pla
	tax
	pla
	rti

// Bottom IRQ
// **********
// Handler called by a raster IRQ
// This routine updates the screen 
.pc=* "Bottom IRQ"	
irq_bottomirq:
	pha
	txa
	pha	
	tya
	pha	
	lda $01
	pha
	lda #$35
	sta $01	
	jsr updposition
	lda posy+1
	eor #$07
	jsr initrtimer
	lda posx+1
	sta coltmp
	and #$07
	eor #[d016val|$07]
	sta $d016	
	lda posx+2
	lsr
	ror coltmp
	lsr
	ror coltmp
	lsr
	ror coltmp		
	lax coltmp
	eor colprc
	stx colprc
	ora rowchg
	beq !+
	ldx coltmp
	ldy rowtmp	
	jsr drawscreen
!:	
	pla
	sta $01
	lax rowchg
	beq !+	
	lda #$00
	sta rowchg	
	jsr decrunchrow
!:
	pla
	tay
	pla
	tax
	pla		
	rti


decrunchrow:
	lda posy+2
	sta tmppos
	lda posy+1
	lsr tmppos
	ror
	lsr tmppos
	ror
	lsr tmppos
	ror
	cpx #$ff
	beq drdown
	cpx #$01
	beq drup
drexit:
	rts
drup:		
	adc #24
	cmp #sizey
	bcc drend
	rts
drdown: 	
	sbc #1
	bcc drexit
drend:
	ldy decidx
	sta declist,y
	iny
	tya
	and #$0f
	sta decidx
	rts
	
// ***************
//  Set first irq
// ***************
setfstirqnmi:
	lda #<nmibl
	sta $fffa
	lda #>nmibl
	sta $fffb	
	lsr $d019
	lda #$01
	sta $d01a
	
// **************
// Set bottom IRQ
// **************
setbotirq:
	lda #$ff
	sta $d012
	lda $d011
	and #$7f
	sta $d011
	lda #<irq_bottomirq
	sta $fffe
	lda #>irq_bottomirq
	sta $ffff
	rts	
	
	
// ***********
// Set SID IRQ
// ***********
setsidirq:
	lda #$00
	sta $d012
	lda $d011
	and #$7f
	sta $d011
	lda #<irq_sidplayer
	sta $fffe
	lda #>irq_sidplayer
	sta $ffff
	rts		

	
