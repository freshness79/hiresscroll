// TIMER SETUP (CIA2/1)
// ********************	
// Prepare CIA 2 timer 1 for counting NMI
// To be called ONCE PER FRAME!


disableTimers:
	sei
	lda #$7f
	sta $dc0d
	sta $dd0d
	bit $dc0d
	bit $dd0d
	lda #$00
	sta $dd0e
	sta $dd0f
	cli
	rts
	
initrtimer:
	sei	
	ora #$18				// X=X+$18
	tax	
	and #$17	
	sta d011val				// Save correct D011 value
	ora #$80	
	sta $d011				// and start frame with it	
	dex						// X=X-1
	lda #$17	
	sax d011dbl			// Save D011 value for skipping BLs
	lda #$1f	
	axs #[$100+$18-$01-$2c]	// X=X-$18+$01+$2c
	stx $d012				// Set timer stabilization raster IRQ
	lda #<irqrtim1
	sta $fffe
	lda #>irqrtim1
	sta $ffff
	lda #$01
	sta $d01a
	lsr $d019
	cli	
	rts


// TIMER 2 Stabilization
// *********************
.pc=* "IRQ Timer 1"
.align $0020
irqrtim1:			// 9 (+7)	
	sta savea		// 3
	stx savex		// 3	
	lda $01			// 3
	pha				// 3
	lda #$35		// 2
	sta $01			// 3
	inc $d012		// 6	
	lda #<irqrtim2	// 2	
	sta $fffe		// 4		
	lda #$7f		// 2
	sta $dd0d		// 4
	tsx				// 2
	lsr $d019		// 6
	cli				// 2	[Shared zone]	
irqrtim2:			// - 9 (+1)				
.if ([>irqrtim1]!=[>irqrtim2]) .error "Timer 1 page crossing!"	
	.fill 9,NOP		// 18	[Shared zone]	
	sty.a savey		// 4
	lda #$00		// 2
	sta $dd0e		// 4
	txs				// 2	
	lda #$81		// 2
	ldy #$00		// 2
	ldx #<[63*8-1]	// 2	
	stx $dd04		// 4	
	ldx #>[63*8-1]	// 2
	stx $dd05		// 4
	ldx $d012		// 4
	cpx $d012		// 4 - 63 (+1)
	beq !+			// 2/3 -> stable
!:						// 2
	bit $dd0d		// 4
	sta $dd0d		// 4	
	lda #06			// 2
	sta startnmi	// 4
	jsr setsidirq	// 6
	lda #$01		// 2
	sta $dd0e		// 4	
	lda #<nmiwt		// 2
	sta $fffa		// 4
	pla				// 4
	sta $01			// 3
	lda savea		// 3
	ldx savex		// 3
	ldy savey		// 3
	rti

