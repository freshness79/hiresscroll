.pc = $0000
.var logo = LoadPicture(cmdLineVars.get("fname"))
.var	X=floor(logo.width/8)+[mod(logo.width,8)!=0?1:0]
.var	Y=floor(logo.height/8)+[mod(logo.height,8)!=0?1:0]
.if (cmdLineVars.get("multi").asNumber()==1)
	.for (var y=0; y<Y; y++)
		.for (var x=0;x<X; x++)
			.for(var charPosY=0; charPosY<8; charPosY++)						
				.byte logo.getMulticolorByte(x,charPosY+y*8)
else 
	.for (var y=0; y<Y; y++)
		.for (var x=0;x<X; x++)
			.for(var charPosY=0; charPosY<8; charPosY++)						
				.byte logo.getSinglecolorByte(x,charPosY+y*8)