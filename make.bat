@echo off
REM Change GOTO and recompile
REM *************************

goto jsl

:jsl
REM Immagine di JSL
set X=800
set Y=1063
set COL1=0
set COL2=1
set MULTI=0
set IMAGE=jsl
goto compute

:comix
REM Immagine di Comix
set X=576
set Y=896
set COL1=0
set COL2=1
set MULTI=0
set IMAGE=comix
goto compute


:strip
REM Immagine Strip
set X=600
set Y=960
set COL1=0
set COL2=1
set MULTI=0
set IMAGE=strip
goto compute

:horse
REM Immagine del cavallo
set X=1024
set Y=960
set COL1=9
set COL2=8
set COL3=1
set COL4=2
set MULTI=1
set IMAGE=horse
goto compute

:robot
REM Immagine del robot
set X=720
set Y=768
set COL1=0
set COL2=9
set COL3=12
set COL4=15
set MULTI=1
set IMAGE=robot
goto compute

:dragon
REM Immagine del dragone
set X=800
set Y=552
set COL1=12
set COL2=8
set COL3=1
set COL4=15
set MULTI=1
set IMAGE=dragon
goto compute

:6569
REM Immagine del chip
set X=512
set Y=512
set COL1=12
set COL2=8
set COL3=1
set COL4=15
set MULTI=1
set IMAGE=6569
goto compute

:rose
REM Immagine della rosa in greyscale
set X=600
set Y=448
set COL1=0
set COL2=9
set COL3=12
set COL4=15
set MULTI=1
set IMAGE=rose
goto compute

:pattern
REM Immagine dei pattern
set X=1000
set Y=544
set COL1=0
set COL2=9
set COL3=12
set COL4=15
set MULTI=1
set IMAGE=pattern
goto compute

:afternoon
REM Immagine tramonto
set X=512
set Y=384
set COL1=9
set COL2=8
set COL3=2
set COL4=7
set MULTI=1
set IMAGE=afternoon
goto compute

:compute
if "%MULTI%"=="0" (
set /a COLOR="COL1 + COL2*16"
) else (
set /a COLOR="COL1 + COL2*16 + COL3*256 + COL4*4096"
)
cd image
call make %IMAGE%.png %IMAGE%.bin %MULTI%
cd ..
cd bin
..\tools\splitter %X% %Y% ..\image\%IMAGE%.bin
cd ..
cd src
call kick viewer.asm :sizex=%X% :sizey=%Y% :multi=%MULTI% :color=%COLOR%
move *.prg ..\build
del *.sym
cd ..\build
..\tools\exomizer sfx $0800 viewer.prg -Di_table_addr=0x2 -s "lda #$00 sta $d011 sta $d020" -X "sty $d020 lda #$00 sta $d020"
del viewer.prg
del %IMAGE%.prg
ren a.out %IMAGE%.prg
cd ..
pause