#include<stdio.h>
#include<string.h>
#include<dos.h>
#include<conio.h>
#include<stdlib.h>
#include<limits.h>
#include"b2\cruncher.h"
#include"b2\file.h"
#include"b2\bb.h"



// Computed constants
// ******************

#define	topgval(X) (((X&0x3800)>>10)+(((X>>14)&2)?0x0:0x1))

// Bank 1-3
unsigned int fonts[]={
	0x4800,0x5000,0x5800,0x6000,0x6800,0x7000,
	0xc800,0xd000,0xd800,0xe000,0xe800,0xf000,0xf800,
	0x4400,0x4c00,0x5400,0x5c00,0x6400,0x6c00,0x7400,
	0xc400,0xcc00,0xd400,0xdc00,0xe400,0xec00,0xf400
};	


//*******************
//*******************

int error(int err)
{
	int ret=0;
	switch(err) {
		case 0: printf("Errore di sintassi\n");
				printf("Usare: splitter <X> <Y> <filename>");
				printf("Valori ammessi:\n");
				printf("\tX: 8-1024\n");
				printf("\tY: 8-2048\n");
				ret=-1;
				break;
		case 1:	printf("Impossibile aprire il file\n");
				ret=-2;
				break;
		case 2: printf("Memoria insufficiente\n");
				ret=-3;
		case 3: printf("Sono stati superati i 65536 bytes\n");
				ret=-4;
	}
	return(ret);
}
	
//*******************
//*******************

int getfreepos(unsigned int *fp,unsigned int *fnts,int n, int count, int scr1, int scr2)
{
	byte buffer[65536L];
	unsigned int tmp;
	long int i,j,k=0;
	bool flag;
	for(i=0;i<65536L;i++)
		buffer[i]=0;
	for(i=0;i<0x280;i++)
		buffer[i]=1;
	for(i=2048;i<8192;i++)
		buffer[i]=1;
	for(i=65530;i<65536L;i++)
		buffer[i]=1;
	for(i=0;i<n;i++)	
		for(j=0;j<8*count;j++)
			buffer[j+fnts[i]]=1;
	for(j=0;j<1000;j++) {
		buffer[scr1+j]=1;
		buffer[scr2+j]=1;
	}
	for(i=0;i<65536L;i++)
		if (!buffer[i]) {
			fp[2*k]=i;
			for(;i<65536L&&!buffer[i];i++)
			fp[2*k+1]=i-fp[2*k]+1;
			k++;
		}
	for(flag=true,i=k;i>0&&flag;i--) {
		flag=false;
		for(j=1;j<k;j++) {
			if (fp[2*(j-1)+1]<fp[2*j+1]) {
				tmp=fp[2*(j-1)];
				fp[2*(j-1)]=fp[2*j];
				fp[2*j]=tmp;
				tmp=fp[2*(j-1)+1];
				fp[2*(j-1)+1]=fp[2*j+1];
				fp[2*j+1]=tmp;
				flag=true;
			}
		}
	}	
	return(k);
}

//*******************
//*******************

long optmemory(unsigned char *data,long int *start,long int *lentbl,byte *buffer,unsigned int blockX,unsigned int blockY)
{
	unsigned long mem,avail,newset,set,lost;
	unsigned int freepos[256];
	char *done;
	long int i,j,k;
	int count,idx;
	done=(char *)malloc(sizeof(char)*blockY);
	if (done==NULL) exit(error(2));	
	for(i=0;i<65536L;i++) 
		buffer[i]=0;
	for(i=0;i<blockY;i++)	
		done[i]=0;		
	count=getfreepos(freepos,fonts,sizeof(fonts)/sizeof(unsigned int),blockX,0x4000,0xc000);
	for(i=0;i<count;i++)
	 printf("Area %04X - Lenght %04X\n",freepos[2*i],freepos[2*i+1]);
	printf("Free areas: %02d\n",count);
	printf("Optimizing free memory");
	for(lost=0,j=0;j<count;j++) {
		avail=freepos[2*j+1];
		mem=freepos[2*j];
		idx=0;
		while(idx>=0) {			
			idx=-1;
			set=ULONG_MAX;				
			for(i=0;i<blockY;i++)
			{
				k=lentbl[i];
				if ((!done[i])&&(k<avail)) {
					newset=avail-k;
					if (newset<set) {						
						set=newset;						
						idx=i;
					}
				}
			}
			if (idx>=0) {
				done[idx]=1;
				for(k=0;k<(avail-set);k++)
					buffer[mem+k]=data[start[idx]+k];
				start[idx]=mem;
				mem+=(avail-set);
				avail=set;
				printf(".");
				
			}
			else lost+=avail;
		}
	}	
	for(k=0,i=0;i<blockY;i++)
		if (done[i])			
			k+=lentbl[i];		
	printf("\nTotal gained: %ld\n",k);
	printf("Total lost: %ld\n",lost);
	for(i=0;i<blockY;i++) {
		buffer[i*2]=start[i]&0xff;
		buffer[i*2+1]=start[i]/256;
	}
	free(done);
	return(k);
}

//*******************
//*******************

int getNumber(char *s)
{
	int val=0,i;
	for(i=0;(s[i]>='0'&&s[i]<='9');i++)
		val=val*10+(s[i]-'0');
	if (s[i]==0) return(val);
			else return(-1);
}



//*******************
//*******************

int main(int argc,char *argv[])
{
	File *rows,source,rsource;
	byte stuffed[65536L];
	byte buffer[65536L];
	long *lentbl;
	long *start;
	unsigned int tmp;	
	long int i,j,y,pos,opt;
	unsigned int blockX,blockY;
	unsigned int sizeX;
	unsigned int sizeY;
	if (argc!=4) return(error(0));
	if (!readFile(&source, argv[3])) return(error(1));
	sizeX=getNumber(argv[1]);
	sizeY=getNumber(argv[2]);	
	rows=(File *)malloc(sizeof(File)*blockY);
	if (rows==NULL) return(error(2));
	if (sizeX<8||sizeX>1024||sizeY<8||sizeY>2048) return(error(0));
	blockX=(int)(sizeX/8+((sizeX%8)?1:0));
	blockY=(int)(sizeY/8+((sizeY%8)?1:0));	
	lentbl=(long *)malloc(sizeof(long)*blockY);
	if (lentbl==NULL) return(error(2));
	start=(long *)malloc(sizeof(long)*blockY);
	if (start==NULL) return(error(2));
	for(i=0;i<13;i++)
		fonts[i]+=0x0400-blockX*8;
	rsource=source;
	rsource.size=blockX*8;
	printf("Preparing rows");
	for(pos=0,y=0;y<blockY;y++)
	{								
		crunch(&rsource,rows+y,0x0000,false,false);							// Comprimo 8 linee (una riga)
		rsource.data+=blockX*8;												// Passo alla prossima riga
		if (rows[y].size+pos>65535) return(error(3));
		for(i=2;i<rows[y].size-2;i++)
			stuffed[pos+i]=rows[y].data[2+i];
		stuffed[pos+0]=fonts[y%(sizeof(fonts)/sizeof(unsigned int))]&0xff;
		stuffed[pos+1]=fonts[y%(sizeof(fonts)/sizeof(unsigned int))]/256;
		start[y]=pos;
		lentbl[y]=rows[y].size-2;
		pos+=rows[y].size-2;
		printf(".");
	}
	freeFile(&source);
	printf("\n");
	printf("Total uncompressed size: %ld\n",blockX*8*blockY);
	printf("Total compressed size: %ld\n",pos);
	printf("Ratio: %5.2f%%\n",(100.0*pos)/(blockX*8*blockY));	
	opt=optmemory(stuffed,start,lentbl,buffer,blockX,blockY);	
	source.data=buffer+8192;
	source.size=65536L-8192-6;
	writeFile(&source,"stuff2000.bin");
	freeName(&source);
	source.data=buffer+0x280;
	source.size=0x580;	
	writeFile(&source,"stuff0280.bin");
	freeName(&source);	
	source.data=buffer;
	source.size=blockY*2;	
	writeFile(&source,"index.bin");
	freeName(&source);
	free(lentbl);
	free(start);
	free(rows);
	if (opt<pos) {
		printf("The compressed file doesn't fit the memory, compressed data is only partial\n");
		return(-1);
	}
	else {
		printf("The compressed file fits the available memory\n");
		return(0);
	}
}


